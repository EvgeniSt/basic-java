# Arrays
## Declare and Initialize
< type > < dimension > < name > = new < type > < dimension size >
## Retrieve by index
< name > < dimension index >
## Array feature
< name >.length
# Iteration
## while
```java
boolean condition;
while (condition){
    //logic
}
```
## do while
```java
boolean condition;
do{
    //logic
}while (condition)
```
## for
```java
boolean condition;
for (int declaredVariables; condition; <someLogic>){
    //logic
}
```
## break;continue;label

# Task
1. In your forked repository create a directory called `examples`
2. **Add files with code:**
    - **for printing a square**
    - ***for printing a square empty inside**
    - **infinite iteration**

Files should be ready to compile like:
```java
package examples;

public class NameOfTheFile {
    public static void main( String[] args ) {
        //your code
    }
}
```

*NameOfTheFile should be changed
3. Commit and push, then create a MR

### Special
code to create United Kingdom flag